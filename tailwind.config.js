module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: (theme) => ({
        wedding: "url('./assets/image.jpg')",
      }),
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
