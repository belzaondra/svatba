import React from "react";
interface MapProps {}

export const Mapa: React.FC<MapProps> = () => {
  return (
    <>
      <iframe
        src="https://frame.mapy.cz/s/hezusovope"
        className="w-full h-96 px-4 mt-12"
        title="map"
      ></iframe>
    </>
  );
};
