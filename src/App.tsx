import { useEffect, useState } from "react";
import ReactGA from "react-ga";
import { AiFillHeart, AiOutlineMenu } from "react-icons/ai";
import { Link } from "react-scroll";
import "./App.css";
import { Mapa } from "./Components/Map";

function App() {
  const [showDropdown, setShowDropdown] = useState<boolean>(false);

  useEffect(() => {
    ReactGA.initialize("UA-193122222-3");

    ReactGA.pageview("/");
  }, []);
  return (
    <div className="App">
      <div className=" mx-auto flex ">
        <div className="w-full md:w-6/12">
          <div className=" md:w-11/12 lg:8/12 xl:w-7/12 mx-auto" id="home">
            {/* Menu telefon */}
            <div className="flex pr-6 md:hidden sticky top-0 z-50 bg-white">
              <Link to="home" className="text-2xl p-4 font-semibold">
                Bára a Jarda
              </Link>
              <AiOutlineMenu
                className="my-4 h-6 w-6 z-50 md:w-0 md:h-0 ml-auto"
                onClick={() => {
                  setShowDropdown(!showDropdown);
                }}
              />
              {showDropdown && (
                <div className="absolute right-6 pt-11 z-40">
                  <div className="bg-white p-2 rounded-md border text-lg border-black">
                    <Link
                      to="misto"
                      smooth={true}
                      className="text-center px-2"
                      onClick={() => setShowDropdown(false)}
                    >
                      Místo
                    </Link>
                    <hr />
                    <Link
                      to="conasceka"
                      smooth={true}
                      className="text-center px-2"
                      onClick={() => setShowDropdown(false)}
                    >
                      Co nás čeká
                    </Link>
                    <hr />
                    <Link
                      to="format"
                      smooth={true}
                      className="text-center px-2"
                      onClick={() => setShowDropdown(false)}
                    >
                      Formát
                    </Link>
                    <hr />
                    <Link
                      to="spani"
                      smooth={true}
                      className="text-center px-2"
                      onClick={() => setShowDropdown(false)}
                    >
                      Spaní
                    </Link>
                    <hr />
                    <Link
                      to="dary"
                      smooth={true}
                      className="text-center px-2"
                      onClick={() => setShowDropdown(false)}
                    >
                      Dary
                    </Link>
                    <hr />
                    <Link
                      to="kontakt"
                      smooth={true}
                      className="text-center px-2"
                      onClick={() => setShowDropdown(false)}
                    >
                      Kontakt
                    </Link>
                  </div>
                </div>
              )}
            </div>

            {/* Menu desktop */}
            <div className="hidden md:block md:sticky bg-white z-50 top-0">
              <div className="flex justify-evenly py-4 text-lg ">
                <Link className="page-link" to="misto" smooth={true}>
                  Místo
                </Link>
                <Link className="page-link" to="conasceka" smooth={true}>
                  Co nás čeká
                </Link>
                <Link className="page-link" to="format" smooth={true}>
                  Formát
                </Link>
                <Link className="page-link" to="spani" smooth={true}>
                  Spaní
                </Link>
                <Link className="page-link" to="dary" smooth={true}>
                  Dary
                </Link>
                <Link className="page-link" to="kontakt" smooth={true}>
                  Kontakt
                </Link>
              </div>
            </div>

            <div className="section items-center">
              <img
                src="detail.jpg"
                alt="detail"
                className="rounded-full w-7/12 block md:hidden"
              />

              <div className="md:h-full  md:mb-44 flex flex-col items-center justify-center">
                <h1 className="text-5xl md:text-7xl text-center font-semibold mt-8 md:mt-0  ">
                  Bára & Jarda
                </h1>
                <div className="flex justify-center md:mr-20 mt-4 text-3xl md:text-4xl text-gray-800">
                  <span>12.6.2021</span>

                  <div className="flex flex-col justify-center mx-5">
                    <div
                      className="bg-gray-800 w-14 mt-2"
                      style={{ height: "1px" }}
                    ></div>
                  </div>

                  <span>16.00</span>
                </div>
              </div>
            </div>

            <div className="section" id="misto">
              {/* <div className="py-14" ></div> */}
              <h3 className="section-title ">
                <span>Místo</span>
              </h3>
              <hr className="title-decoration " />
              <p className="section-text">
                Obora u Jemniště <br /> obřad, zahradní slavnost, svatební
                hostina
                <br />
                parkování zajištěno
              </p>
              <Mapa />
            </div>

            <div className="section" id="conasceka">
              <h3 className="section-title">
                <span>Co nás čeká</span>
              </h3>
              <hr className="title-decoration" />
              <div className="section-text">
                <p>14.00 - 15.00 – příjezd hostů na Oboru, uvítání</p>
                <p className="my-2">16.00 – svatební obřad</p>
                <p className="my-2">17.00 – dort, přípitek a společné focení</p>
                <p className="my-2">18.00 – hostina - raut</p>
                <p className=" my-2">20.00 – kapela, tanec, zpěv</p>
                <br />
                <p>
                  a někdy mezi tím házení svatební kytice, <br />
                  proslovy, koupání v rybníce, kubb a další veselí
                </p>
              </div>
            </div>

            <div className="section" id="format">
              <h3 className=" section-title">
                <span>Formát</span>
              </h3>
              <hr className="title-decoration" />
              <p className="section-text">
                Příjemně neformální zahradní slavnost s obřadem venku pod širým
                nebem s hostinou a koncertem ve stodole. Pestrobarevná a veselá,
                akorát vážná a navždycky významná událost v našich životech,
                kterou chceme oslavit s našimi rodinami a kamarády. Oblečte se
                pohodlně a zároveň slavnostně. Taneční boty s sebou!
              </p>
            </div>

            <div className="section" id="spani">
              <h3 className="section-title">
                <span>Spaní</span>
              </h3>
              <hr className="title-decoration" />
              <p className="section-text">
                Ubytování zajištěno pro nejbližší rodinu,
                <br /> ostatní prosíme, vezměte si stan.
              </p>
            </div>

            <div className="section" id="dary">
              <h3 className="section-title">
                <span>Dary</span>
              </h3>
              <hr className="title-decoration" />
              <p className="section-text">
                Budeme rádi za jakékoli dary, finanční či hmotné, ale hlavně
                takové, z kterých budeme mít radost.
              </p>
            </div>

            <div className="section" id="kontakt">
              <h3 className="section-title">
                <span>Kontakt</span>
              </h3>
              <hr className="title-decoration" />
              <div className="section-text">
                <p>Bára – 774 848 759 – b.belzova@gmail.com</p>
                <p>Jarda – 605 709 354 – dedic.jd@gmail.com</p>
              </div>

              <br />
              <div className="flex flex-col section-text">
                <p>a naši milí svědci:</p>
                <p>Tomáš Kučera – 732 100 723</p>
                <p>Martina Houšková – 777 647 065</p>
              </div>
            </div>
            <p className="text-center py-4 footer flex justify-center items-center">
              Made with
              <AiFillHeart className="inline-block text-red-500 max-h-5 w-5 mx-2" />
              Ondřej Belza
            </p>
          </div>
        </div>

        {/* Image section */}
        <div className="w-0 md:w-6/12  h-screen sticky top-0">
          <div className="bg-wedding h-screen w-full"></div>
        </div>
      </div>
    </div>
  );
}

export default App;
